import React, { useEffect, useState } from 'react';
import { FontAwesome } from '@expo/vector-icons';
import {
  View,
  Text,
  SafeAreaView,
  Image,
  ScrollView,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import { MAP_API_KEY } from '@env';

import AvatarImg from '../../assets/avatar.png';
import HotelsImg from '../../assets/hotel.png';
import AttractionsImg from '../../assets/attraction.png';
import RestaurantImg from '../../assets/restaurants.png';
import NotFoundImg from '../../assets/NotFound.png';

import MenuContainer from '../../components/MenuContainer';
import Card from '../../components/Card';
import { getPlacesData } from '../../api';

const menu = [
  {
    title: 'Hotels',
    key: 'hotels',
    imageSrc: HotelsImg,
  },
  {
    title: 'Attractions',
    key: 'attractions',
    imageSrc: AttractionsImg,
  },
  {
    title: 'Restaurants',
    key: 'restaurants',
    imageSrc: RestaurantImg,
  },
];

const DiscoverScreen = () => {
  const [type, setType] = useState('restaurants');
  const [isLoading, setIsLoading] = useState(false);
  const [mainData, setMainData] = useState([]);

  useEffect(() => {
    setIsLoading(true);
    fetchData();
  }, []);

  const fetchData = async () => {
    getPlacesData().then((data) => {
      setMainData(data);
      setInterval(() => {
        setIsLoading(false);
      }, 2000);
    });
  };

  return (
    <SafeAreaView className='relative flex-1 bg-white'>
      {/* HEADER CONTAINER */}
      <View className='flex-row items-center justify-between px-8'>
        <View>
          <Text className='text-[40px] text-[#0B646B] font-bold'>Discover</Text>
          <Text className='text-[36px] text-[#527283] font-normal'>
            the beauty today
          </Text>
        </View>

        <View className='w-12 h-12 rounded-md items-center justify-center shadow-lg'>
          <Image
            source={AvatarImg}
            className='object-cover w-full h-full rounded-md'
          />
        </View>
      </View>

      {/* SEARCH FIELD  */}
      <View className='flex-row items-center bg-white mx-4 rounded-xl py-1 px-4 shadow-lg'>
        <GooglePlacesAutocomplete
          GooglePlacesDetailsQuery={{ fields: 'geometry' }}
          placeholder='Search'
          onPress={(data, details = null) => {
            // 'details' is provided when fetchDetails = true
            console.log(details?.geometry?.viewport);
          }}
          query={{
            key: MAP_API_KEY,
            language: 'en',
          }}
        />
      </View>

      {/* MENU CONTAINER */}
      {isLoading ? (
        <View className='flex flex-1 items-center justify-center'>
          <ActivityIndicator size='large' color='#0B646B' />
        </View>
      ) : (
        <ScrollView>
          <View className='flex flex-row items-center justify-between px-8 mt-8'>
            {menu.map(({ key, title, imageSrc }) => (
              <MenuContainer
                key={key}
                title={title}
                imageSrc={imageSrc}
                type={type}
                setType={setType}
              />
            ))}
          </View>

          <View>
            <View className='flex flex-row items-center justify-between px-4 mt-8'>
              <Text className='text-[#2C7379] text-[28px] font-bold'>
                Top Tips
              </Text>
              <TouchableOpacity className='flex flex-row items-center justify-center space-x-2'>
                <Text className='text-[#2C7379] text-[20px] font-bold'>
                  Explore{' '}
                </Text>
                <FontAwesome
                  name='long-arrow-right'
                  size={24}
                  color='#A0C4C7'
                />
              </TouchableOpacity>
            </View>

            <View className='px-4 mt-8 flex flex-row items-center justify-evenly flex-wrap'>
              {mainData?.length > 0 ? (
                <>
                  {mainData?.map((data, i) => (
                    <Card
                      key={i}
                      location={data?.location_string | ''}
                      title={data?.name || ''}
                      imageSrc={
                        data?.photo?.images?.medium?.url
                          ? data?.photo?.images?.medium?.url
                          : 'https://cdn.pixabay.com/photo/2015/10/30/12/22/eat-1014025_1280.jpg'
                      }
                    />
                  ))}
                </>
              ) : (
                <View className='w-full h-[400px] items-center justify-center space-y-8'>
                  <Image
                    source={NotFoundImg}
                    alt='empty-data'
                    className='w-32 h-32 object-cover'
                  />
                  <Text className='text-2xl text-[#428288] font-semibold'>
                    Oops...No data found
                  </Text>
                </View>
              )}
            </View>
          </View>
        </ScrollView>
      )}
    </SafeAreaView>
  );
};

export default DiscoverScreen;
