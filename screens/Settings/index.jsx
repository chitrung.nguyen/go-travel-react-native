import { SafeAreaView, Text, Button } from 'react-native';
import React from 'react';

const SettingsScreen = ({ navigation }) => {
  return (
    <SafeAreaView className='flex-1 items-center justify-center bg-white'>
      <Text>SettingsScreen</Text>
      <Button title='Go back' onPress={() => navigation.navigate('Home')} />
    </SafeAreaView>
  );
};

export default SettingsScreen;
