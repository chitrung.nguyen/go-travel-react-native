import { useNavigation } from '@react-navigation/native';
import React from 'react';
import { SafeAreaView, Text, TouchableOpacity, View } from 'react-native';
import * as Animatable from 'react-native-animatable';

import HeroImg from '../../assets/hero.png';

const HomeScreen = () => {
  const navigation = useNavigation();

  return (
    <SafeAreaView className='relative flex-1 bg-white'>
      {/* FIRST SECTION */}
      <View className='flex-row px-6 mt-8 items-center space-x-2 '>
        <View className='w-16 h-16 bg-black rounded-full items-center justify-center'>
          <Text className='text-[#00BCC9] text-3xl font-semibold'>Go</Text>
        </View>
        <Text className='text-[#2A2B4B] text-3xl font-semibold'>Travel</Text>
      </View>

      {/* SECOND SECTION */}
      <View className='px-6 mt-8 space-y-3'>
        <Text className='text-[#3C6072] text-[42px]'>Enjoy the trip with</Text>
        <Text className='text-[#00BCC9] text-[38px] font-bold'>
          Good Moments
        </Text>
        <Text className='text-[#3C6072] text-base'>
          Lorem Ipsum is simply dummy text of the printing and typesetting
          industry.
        </Text>
      </View>

      {/* CIRCLE SECTION */}
      <View className='w-[400px] h-[400px] bg-[#00BCC9] rounded-full absolute bottom-36 -right-36'></View>
      <View className='w-[400px] h-[400px] bg-[#E99265] rounded-full absolute -bottom-28 -left-36'></View>

      {/* HERO IMAGE */}
      <View className='flex-1 relative items-center justify-center'>
        <Animatable.Image
          animation='pulse'
          easing='ease-in-out'
          source={HeroImg}
          className='object-cover w-full h-full mt-20'
        />
        {/* BUTTON */}
        <TouchableOpacity
          onPress={() => navigation.navigate('DiscoverScreen')}
          className='absolute bottom-20 w-24 h-24 border-l-2 border-r-2 border-t-4 border-[#00BCC9] rounded-full items-center justify-center'
        >
          <Animatable.View
            animation='pulse'
            easing='ease-in-out'
            iterationCount='infinite'
            className='w-20 h-20 items-center justify-center rounded-full bg-[#00BCC9]'
          >
            <Text className='text-gray-50 text-[36px] font-semibold'>Go</Text>
          </Animatable.View>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

export default HomeScreen;
