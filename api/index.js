import axios from 'axios';

const headers = {
  'X-RapidAPI-Key': 'b8925170c9mshd5f82b67ae0f3e7p15e9b6jsn1d5e37824240',
  'X-RapidAPI-Host': 'travel-advisor.p.rapidapi.com',
};

export const getPlacesData = async () => {
  try {
    const { data: { data: dataResponse } = {} } = await axios.get(
      `https://travel-advisor.p.rapidapi.com/restaurants/list-in-boundary`,
      {
        headers: {
          'X-RapidAPI-Key':
            'b8925170c9mshd5f82b67ae0f3e7p15e9b6jsn1d5e37824240',
          'X-RapidAPI-Host': 'travel-advisor.p.rapidapi.com',
        },
        params: {
          bl_latitude: '11.847676',
          tr_latitude: '12.838442',
          bl_longitude: '109.095887',
          tr_longitude: '109.149359',
          restaurant_tagcategory_standalone: '10591',
          restaurant_tagcategory: '10591',
          limit: '30',
          currency: 'USD',
          open_now: 'false',
          lunit: 'km',
          lang: 'en_US',
        },
      }
    );

    return dataResponse;
  } catch (error) {
    console.log('Error getPlacesData: ', error);
    return null;
  }
};
